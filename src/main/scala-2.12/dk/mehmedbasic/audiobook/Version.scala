package dk.mehmedbasic.audiobook

/**
  * Version object for printing the version of the application
  *
  * @author Jesenko Mehmedbasic
  *         created 19-01-13, 12:42
  */
object Version {
  val versions = Array("1.0d", "1.0c", "1.0b")

  def currentVersion(): String = versions(0)

  def allVersions: Array[String] = versions
}
