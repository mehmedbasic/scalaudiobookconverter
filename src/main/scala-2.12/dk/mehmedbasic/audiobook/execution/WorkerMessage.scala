package dk.mehmedbasic.audiobook.execution

import java.io.File

import dk.mehmedbasic.audiobook.conversion.ConversionType

/**
  * A worker message
  *
  * @author JEKM
  *         created 05-05-2014, 18:13
  */
trait WorkerMessage {

}

object WorkerMessage {

  case class Calculate(inputFiles: List[File], outputFiles: List[File]) extends WorkerMessage

  case class Work(input: File) extends WorkerMessage

  case class Result(input: File, dest: File) extends WorkerMessage

  case class ConversionFinished(
                                 conversionType: ConversionType,
                                 inputFiles: List[File],
                                 result: List[Result],
                                 outputFiles: List[File]) extends WorkerMessage

  case object CleanUp extends WorkerMessage

}
