package dk.mehmedbasic.audiobook.execution

/**
  * A command parameter.
  *
  * @author Jesenko Mehmedbasic
  *         created 15-01-13, 22:08
  */
class Parameter(switch: String, value: Any) {

  protected[execution] def this(switch: String) = this(switch, null)

  def getSwitch: String = switch

  def getValue: String = valueToString


  def valueToString: String = {
    if (value == null) {
      return null
    }
    value.toString
  }

  def asString(switchToken: String): String = {
    if (switch == null) {
      return getValue
    }
    if (getValue == null) {
      return switchToken + getSwitch
    }
    switchToken + getSwitch + " " + getValue
  }
}
