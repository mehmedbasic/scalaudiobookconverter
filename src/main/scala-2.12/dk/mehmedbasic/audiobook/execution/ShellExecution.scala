package dk.mehmedbasic.audiobook.execution

import java.util

/**
  * Executes a command.
  * Can vary by operating system.
  */
class ShellExecution {
  def execute(command: Command): Process = {
    val list: util.ArrayList[String] = new util.ArrayList[String]()
    list.add(command.getExecutable)
    command.parametersAsArray("-").foreach(s => list add s)

    new ProcessBuilder(list)
      .redirectErrorStream(true)
      .start()
  }
}
