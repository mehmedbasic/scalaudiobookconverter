package dk.mehmedbasic.audiobook.execution

/**
  * TODO - someone remind me to document this class
  *
  * @author Jesenko Mehmedbasic
  *         created 6/8/2015.
  */
object OperatingSystem {
  def locate: OperatingSystem = {
    val property: String = System.getProperty("os.name")
    if (property.contains("Windows")) {
      return Windows
    }
    Unix
  }
}

sealed trait OperatingSystem {
  def shellExecution(): ShellExecution
}

case object Windows extends OperatingSystem {
  override def shellExecution(): ShellExecution = new ShellExecution
}

case object Unix extends OperatingSystem {
  override def shellExecution(): ShellExecution = new ShellExecution
}


