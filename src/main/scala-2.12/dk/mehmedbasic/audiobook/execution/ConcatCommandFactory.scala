
package dk.mehmedbasic.audiobook.execution

import java.io.{File, FileOutputStream, PrintStream}

/**
  * A command factory for the concat command.
  *
  * @author Jesenko Mehmedbasic
  *         created 25-01-13, 01:27
  */
class ConcatCommandFactory(convertedFiles: List[File], output: File) extends CommandFactory {
  val fileList: File = createFileList()

  protected def createWindowsCommand(): Command = {
    new Command("\"" + new File(".").getAbsolutePath + "\\external\\ffmpeg.exe\"")
  }

  protected def createUnixCommand(): Command = new Command("ffmpeg")

  protected def addParametersToCommand(command: Command) :Unit={
    command.addParameter("f", "concat")
    command.addParameter("i", "\"" + fileList.getAbsolutePath + "\"")
    command.addParameter("c", "copy")
    command.addParameter(null, "\"" + temporaryOutput + "\"")
  }

  def createFileList(): File = {
    val inputFiles = File.createTempFile(System.nanoTime() + "_files", ".input")
    val stream: PrintStream = new PrintStream(new FileOutputStream(inputFiles))
    convertedFiles foreach (file => {
      stream.print("file '")
      stream.print(file.getAbsolutePath)
      stream.println("'")
    })
    stream.close()
    inputFiles
  }

  def getFileList: File = fileList

  def temporaryOutput: String = output.getAbsolutePath + ".m4a"

}
