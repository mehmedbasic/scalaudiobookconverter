package dk.mehmedbasic.audiobook.execution

import dk.mehmedbasic.audiobook.io.StreamReader

/**
  * The command factory abstract class.
  *
  * Handles variability in operatingsystems.
  *
  * @author Jesenko Mehmedbasic
  *         created 25-01-13, 01:27
  */
abstract class CommandFactory {
  def createCommand(): Command = {
    val command: Command = OperatingSystem.locate match {
      case Windows => createWindowsCommand()
      case Unix => createUnixCommand()
    }
    addParametersToCommand(command)
    command
  }

  protected def createWindowsCommand(): Command

  protected def createUnixCommand(): Command

  protected def addParametersToCommand(command: Command):Unit


  def createProcess(): Process = {
    val system: OperatingSystem = OperatingSystem.locate
    val executor: ShellExecution = system.shellExecution()
    val process: Process = executor.execute(createCommand())

    // Read stdout and stderr
    new StreamReader(process.getInputStream).start()

    process
  }
}
