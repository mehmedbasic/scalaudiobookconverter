package dk.mehmedbasic.audiobook.execution

import java.io.File

import dk.mehmedbasic.audiobook.conversion.AudioConfig


/**
  * Command factory creates commands based on OS.
  *
  * @author Jesenko Mehmedbasic
  *         created 20-01-13, 00:16
  */
class ConversionCommandFactory(
                                inputFile: File,
                                outputConfig: AudioConfig) extends CommandFactory {

  def createTempFile(): String = {
    var file = File.createTempFile("scab_temp_", ".mp4")
    while (!file.exists()) {
      file = File.createTempFile("scab_temp_", ".mp4")
    }

    val result = file.getAbsolutePath

    file.delete()

    result
  }

  def getOutput: String = createTempFile()

  protected override def addParametersToCommand(command: Command): Unit = {
    command.addParameter("f", "u8")
    command.addParameter("i", "pipe:0")
    command.addParameter("ar", outputConfig.sampleFrequency)
    command.addParameter("ac", outputConfig.channels)
    command.addParameter("f", "mp4")
    command.addParameter(null,  getOutput )
  }

  protected override def createWindowsCommand(): Command = {
    new Command("\"" + new File(".").getAbsolutePath + "\\external\\faac.exe\"")
  }

  protected override def createUnixCommand(): Command = {
    new Command("ffmpeg")
  }
}
