package dk.mehmedbasic.audiobook.execution

import scala.collection.mutable

/**
  * TODO[jekm] - someone remind me to document this class.
  *
  * @author Jesenko Mehmedbasic
  *         created 15-01-13, 22:04
  */
class Command(executable: String) {
  def parametersAsArray(s: String): Array[String] = {
    var size = 0
    parameters.foreach((parameter: Parameter) => size += parameter.asString("-").split(" ").length)
    val array: Array[String] = new Array[String](size)
    var index = 0
    parameters.foreach(
      (parameter: Parameter) => {
        if (parameter.getSwitch != null) {
          array(index) = "-" + parameter.getSwitch
          index += 1
        }
        if (parameter.getValue != null) {
          array(index) = parameter.getValue
          index += 1
        }
      })
    array
  }

  private val parameters: mutable.MutableList[Parameter] = new mutable.MutableList[Parameter]()

  def getExecutable: String = executable

  def addParameter(switch: String, value: Any): Unit = parameters += new Parameter(switch, value)
}
