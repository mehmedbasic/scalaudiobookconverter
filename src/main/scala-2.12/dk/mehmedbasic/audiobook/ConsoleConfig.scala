package dk.mehmedbasic.audiobook

import java.io.{File, FileFilter}

import scala.collection.mutable
import scala.util.matching.Regex

/**
  * The console config containing the parsed commandline parameters.
  *
  * @author Jesenko Mehmedbasic
  *         created 19-01-13, 15:41
  */
case class ConsoleConfig(
                          inputs: mutable.MutableList[File] = new mutable.MutableList[File],
                          outputs: mutable.MutableList[File] = new mutable.MutableList[File]) {
  var graphical: Boolean = false
  var threads: Int = Runtime.getRuntime.availableProcessors()

  def isGraphical: Boolean = graphical

  def setGraphical(): ConsoleConfig = {
    graphical = true
    this
  }

  def outputFiles: List[File] = outputs.toList


  def inputFiles: List[File] = inputs.toList.sortWith(naturalSort)


  def addInputFile(file: File): ConsoleConfig = {
    if (file.isDirectory) {
      file.listFiles(AllowedFiles).foreach(f => inputs += f)
    } else {
      inputs += file
    }
    this
  }

  def addOutputFile(file: File): ConsoleConfig = {
    outputs += file
    this
  }

  object AllowedFiles extends FileFilter {
    val filePattern: Regex = "(?i).*\\.mp3".r

    override def accept(pathname: File): Boolean = {
      if (pathname.isDirectory) {
        return false
      }
      filePattern.findAllIn(pathname.getName).nonEmpty
    }
  }

  def setThreadCount(count: Int): ConsoleConfig = {
    threads = count
    this
  }

  def threadCount(): Int = this.threads

  def naturalSort(f1: File, f2: File): Boolean = {
    val reg = "^[0-9]*".r
    val str1 = f1.getName
    val str2 = f2.getName

    def startsWithNumber(s: String) = s.charAt(0).isDigit

    def leadingNumber(s: String) = reg.findFirstIn(s).get.toDouble

    if (startsWithNumber(str1) && startsWithNumber(str2)) {
      return leadingNumber(str1) < leadingNumber(str2)
    }
    str1 < str2
  }


}
