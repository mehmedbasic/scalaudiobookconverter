package dk.mehmedbasic.audiobook.event

import dk.mehmedbasic.audiobook.HasLogger

import scala.collection.Iterable

/**
  * A logging event subscriber.
  *
  * Every time an event happens it logs it to Log4j
  *
  * @author Jesenko Mehmedbasic
  *         created 5/5/14, 10:39 PM
  */
class LoggingEventSubscriber extends EventSubscriber with HasLogger {
  def subscribesTo(): Iterable[EventType.Value] = EventType.values

  def onEvent(e: ScabEvent) : Unit={
    e.eventType match {
      case EventType.Message => logger().info(e.message)
      case EventType.Error => logger().error(e.message, e.throwable)
      case EventType.FileTick => logger().info("Conversion of '" + e.file + "' percentage: " + e.dataObject)
      case EventType.FileStart => logger().info("Conversion of '" + e.file + "' started.")
      case EventType.FileFinished => logger().info("Conversion of '" + e.file + "' finished successfully.")
      case EventType.JoiningFiles => logger().info("Joining/merging files to destination " + e.file)
      case EventType.JoiningFinished => logger().info("Joining/merging finished successfully to destination " + e.file)
    }
  }
}
