package dk.mehmedbasic.audiobook.event

object EventType extends Enumeration {
  val Message, Error, FileStart, FileTick, FileFinished, JoiningFiles, JoiningFinished = Value
}
