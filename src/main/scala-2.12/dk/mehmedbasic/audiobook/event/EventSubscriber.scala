package dk.mehmedbasic.audiobook.event

import scala.collection.Iterable

trait EventSubscriber {
  ScabEventBus.subscribe(this)

  def subscribesTo(): Iterable[EventType.Value]

  def onEvent(event: ScabEvent):Unit
}
