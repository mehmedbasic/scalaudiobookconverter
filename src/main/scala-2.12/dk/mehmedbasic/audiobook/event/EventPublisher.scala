package dk.mehmedbasic.audiobook.event

import java.io.File

/**
  * A simple event publisher
  */
trait EventPublisher {
  def publish(e: ScabEvent): Unit = ScabEventBus.publish(e)

  def conversionStarted(file: File): Unit = publish(ScabEvent(EventType.FileStart, null, file, null, null))

  def conversionEnded(file: File): Unit = publish(ScabEvent(EventType.FileFinished, null, file, null, null))

  def conversionTicking(file: File, percentage: Double): Unit = publish(
    ScabEvent(EventType.FileTick, null, file, null, percentage))

  def message(file: File, message: String): Unit = publish(ScabEvent(EventType.Message, message, file, null, null))

  def error(file: File, message: String, t: Throwable): Unit = publish(ScabEvent(EventType.Error, message, file, t, null))

  def joining(file: File): Unit = publish(ScabEvent(EventType.JoiningFiles, null, file, null, null))

  def joiningFinished(file: File): Unit = publish(ScabEvent(EventType.JoiningFinished, null, file, null, null))
}
