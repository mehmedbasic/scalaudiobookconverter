package dk.mehmedbasic.audiobook.event

import java.io.File

final case class ScabEvent(eventType: EventType.Value, message: String, file: File, throwable: Throwable, dataObject: Any) {


}

