package dk.mehmedbasic.audiobook.event

import akka.event.{EventBus, LookupClassification}

class ScabEventBus extends EventBus with LookupClassification {
  type Event = ScabEvent
  type Classifier = EventType.Value
  type Subscriber = EventSubscriber

  def mapSize() = 128

  def compareSubscribers(a: EventSubscriber, b: EventSubscriber) = 0

  def classify(event: ScabEvent): EventType.Value = event.eventType

  def publish(event: ScabEvent, subscriber: EventSubscriber): Unit = subscriber.onEvent(event)

  override def subscribe(subscriber: EventSubscriber, to: ScabEventBus#Classifier): Boolean = super.subscribe(subscriber, to)
}

object ScabEventBus {
  private val bus = new ScabEventBus

  init()

  def publish(e: ScabEvent): Unit = bus.publish(e)

  def subscribe(subscriber: EventSubscriber) :Unit={
    subscriber.subscribesTo().foreach(t => bus.subscribe(subscriber, t))
  }

  private def init() :Unit={
    new LoggingEventSubscriber
  }
}
