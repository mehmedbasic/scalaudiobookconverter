package dk.mehmedbasic.audiobook

import org.apache.log4j.Logger


/**
  * Helps printing lines to the console
  *
  * @author Jesenko Mehmedbasic
  *         created 19-01-13, 12:38
  */
trait ConsoleUtil {
  var lineLength = 0
  var errorLine: Int = 0

  def header(): Unit = {
    printLine("ScalaudioBookConverter v" + Version.currentVersion() + " (c) 2013 Jesenko Mehmedbasic")
  }

  def horizontalLine(): Unit = {
    val builder: StringBuilder = new StringBuilder
    for (i <- 1 to lineLength) {
      builder.append("-")
    }
    printLine(builder.toString())
  }

  def error(message: String): Unit = {
    errorLine = errorLine + 1
    logger().error("Error " + errorLine + ": " + message)
  }


  def printLine(string: String): Unit = {
    logger().info(string)
    lineLength = scala.math.max(lineLength, string.length)
  }

  def logger(): Logger
}
