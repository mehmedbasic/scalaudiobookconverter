package dk.mehmedbasic.audiobook

import java.io.File
import java.util.concurrent._

import dk.mehmedbasic.audiobook.conversion.AudioConversion
import org.apache.log4j.Logger
import scopt.OptionParser

import scala.collection.convert.Wrappers.JConcurrentMapWrapper

/**
  * The main class of the converter
  *
  * @author Jesenko Mehmedbasic
  *         created 09-01-13, 03:24
  */
object Scab extends ConsoleUtil {

  LoggerInitialize.init()

  def logger(): Logger = Logger.getLogger("SCAB")

  val finishedFiles = new JConcurrentMapWrapper[Int, File](new ConcurrentHashMap[Int, File]())

  val start: Long = System.currentTimeMillis()

  def main(args: Array[String]): Unit = {

    header()
    horizontalLine()

    val parser: OptionParser[ConsoleConfig] = createParser()
    parser.parse(args, ConsoleConfig()) foreach {
      config =>
        if (config.isGraphical) {
          printLine("Graphical user interface flag set.")
          printLine("Looking up and running GUI")
        } else {

          if (config.outputFiles.isEmpty) {
            error("No output file set.")
            System.exit(-1)
          }
          val files: List[File] = config.inputFiles
          if (!filesExist(files)) {
            System.exit(-1)
          }

          printLine("Running workers, max thread count: " + config.threadCount())
          printLine("Number of input files: " + config.inputFiles.length)

          AudioConversion(config.threadCount(), config.inputFiles, config.outputFiles)
        }
    }
  }

  def stop(): Unit = {
    val stop = System.currentTimeMillis() - start
    val seconds = stop / 1000d
    logger().info("Converted in " + seconds)
  }

  def createParser(): scopt.OptionParser[ConsoleConfig] = {
    new scopt.OptionParser[ConsoleConfig]("ScalaudioBookConverter") {
      opt[File]('o', "output file/directory") unbounded() action {
        (f, config) => {
          config.addOutputFile(f)
        }
      }
      arg[File]("input file(s)") unbounded() action {
        (f, config) => {
          config.addInputFile(f)
        }
      }
    }
  }

  def filesExist(args: List[File]): Boolean = {
    val filtered = args.filterNot((x: File) => x.exists())
    filtered.foreach((x: File) => error("File '" + x.getAbsoluteFile + "' does not exist"))

    filtered.isEmpty
  }

}
