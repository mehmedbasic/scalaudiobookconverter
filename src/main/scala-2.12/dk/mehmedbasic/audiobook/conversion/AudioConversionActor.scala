package dk.mehmedbasic.audiobook.conversion

import java.io.File

import akka.actor.{Actor, ActorContext, ActorRef, ActorSystem, Props}
import akka.routing.{ActorRefRoutee, RoundRobinRoutingLogic, Router}
import dk.mehmedbasic.audiobook.Scab
import dk.mehmedbasic.audiobook.execution.WorkerMessage.{Calculate, ConversionFinished, Result, Work, _}

import scala.collection.mutable


/**
  * This actor defines a single conversion.
  */
class AudioConversionActor(numberOfThreads: Int, finishListener: ActorRef) extends Actor {
  private def makeRouter(context: ActorContext): Router = {
    val routees = Vector.fill(numberOfThreads) {
      val routee = context.actorOf(Props(classOf[SingleFileConversion]))
      ActorRefRoutee(routee)
    }

    Router(RoundRobinRoutingLogic(), routees)
  }

  val system = ActorSystem("scab")
  val router: Router = makeRouter(context)

  class State {
    var conversionType = ConversionType.Joining
    var expectedFiles: List[File] = _
    var outputFiles: List[File] = _
    var actualFiles = 0

    val convertedFiles: mutable.MutableList[Result] = new mutable.MutableList[Result]
  }

  val state = new State

  override def receive: Receive = {
    case Calculate(inputFiles: List[File], outputFiles: List[File]) =>
      outputFiles.size match {
        case 1 =>
          if (outputFiles.head.isDirectory) {
            state.conversionType = ConversionType.Batch
          }
        case _ => state.conversionType = ConversionType.Batch
      }
      state.outputFiles = outputFiles
      state.expectedFiles = inputFiles

      inputFiles foreach (file => router.route(Work(file), sender()))

    case Result(input: File, output: File) =>
      state.actualFiles += 1
      state.convertedFiles += Result(input, output)
      if (state.actualFiles == state.expectedFiles.size) {
        finishListener ! ConversionFinished(state.conversionType, state.expectedFiles, state.convertedFiles.toList, state.outputFiles)
      }

    case CleanUp =>
      Scab.stop()
      system.terminate()
  }
}
