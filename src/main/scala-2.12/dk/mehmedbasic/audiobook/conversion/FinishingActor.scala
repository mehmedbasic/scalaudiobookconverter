package dk.mehmedbasic.audiobook.conversion

import java.io.File

import akka.actor.Actor
import dk.mehmedbasic.audiobook.execution.WorkerMessage.{CleanUp, ConversionFinished, Result}

/**
  * This actor is the finish conversion listener.
  */
class FinishingActor extends Actor with ConvertsResults with Joining with Batch {
  override def receive: Receive = {
    case ConversionFinished(conversionType: ConversionType, i: List[File], r: List[Result], output: List[File]) =>
      conversionType match {
        case ConversionType.Joining =>
          mergeFiles(convertResult(i, r), output.head)
        case ConversionType.Batch =>
          batchComplete(i, r, output)
      }
      sender ! CleanUp
  }


}
