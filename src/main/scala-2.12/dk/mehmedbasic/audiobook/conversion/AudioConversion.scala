package dk.mehmedbasic.audiobook.conversion

import java.io.File

import akka.actor.{ActorRef, ActorSystem, Props}
import dk.mehmedbasic.audiobook.execution.WorkerMessage.Calculate

/**
  * An audio conversion.
  *
  * Bootstraps the akka system and runs the conversion.
  */
object AudioConversion {
  def apply(threads: Int, inputFiles: List[File], outputFiles: List[File]) : Unit={
    val system = ActorSystem.create("ScabSystem")
    val finishListener: ActorRef = system.actorOf(Props(new FinishingActor))
    val masterActor: ActorRef = system.actorOf(Props(new AudioConversionActor(threads, finishListener)), "master")
    masterActor ! Calculate(inputFiles, outputFiles)
  }
}
