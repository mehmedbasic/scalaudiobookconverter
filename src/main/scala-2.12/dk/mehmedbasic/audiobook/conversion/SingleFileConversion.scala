package dk.mehmedbasic.audiobook.conversion

import java.io._
import javazoom.jl.decoder.{Bitstream, Header}
import javazoom.jl.player.{AudioDeviceBase, Player}

import akka.actor.Actor
import dk.mehmedbasic.audiobook.event.EventPublisher
import dk.mehmedbasic.audiobook.execution.WorkerMessage.{Result, Work}
import dk.mehmedbasic.audiobook.execution._
import dk.mehmedbasic.audiobook.io.CountingInputStream

/**
  * Converts a source file to an mp4 aac audio file.
  *
  * @author Jesenko Mehmedbasic
  *         created 09-01-13, 21:06
  */
//noinspection LanguageFeature
class SingleFileConversion extends Actor with EventPublisher {

  def convert(inputFile: File): File = {
    val config: AudioConfig = determineConfig(inputFile)
    val factory: ConversionCommandFactory = new ConversionCommandFactory(inputFile, config)

    conversionStarted(inputFile)

    val process = factory.createProcess()
    val deviceStream = process.getOutputStream
    val device = new OutputStreamAudioDevice(deviceStream)
    val player: Player = new Player(inputFile, device)
    player.play()

    conversionEnded(inputFile)
    new File(factory.getOutput)
  }

  override def receive: Receive = {
    case Work(inputFile: File) =>
      context.parent ! Result(inputFile, convert(inputFile))
  }


  def determineConfig(file: File): AudioConfig = {
    try {
      val sourceStream: BufferedInputStream = new BufferedInputStream(new FileInputStream(file))
      val stream: Bitstream = new Bitstream(sourceStream)
      val header: Header = stream.readFrame

      val channels = if (header.mode() == Header.SINGLE_CHANNEL) {
        1
      } else {
        2
      }

      stream.close()
      AudioConfig(channels, header.frequency())
    } catch {
      case e: Exception => throw new RuntimeException(e)
    }
  }

  implicit def fileToInputStream(file: File): InputStream = new CountingInputStream(file)

  implicit def conversionCommandFactoryToAudioDeviceBase(factory: ConversionCommandFactory): AudioDeviceBase
  = new OutputStreamAudioDevice(factory.createProcess().getOutputStream)
}
