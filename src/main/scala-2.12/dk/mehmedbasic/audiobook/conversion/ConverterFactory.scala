package dk.mehmedbasic.audiobook.conversion

import java.io.File


/**
  * A factory for creating converters.
  */
class ConverterFactory(inputFile: File, finishListener: (String, File) => Unit) {

  def createConverter(): Unit = {
    // TODO
    new SingleFileConversion()
  }
}
