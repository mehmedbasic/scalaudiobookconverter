package dk.mehmedbasic.audiobook.conversion;

/**
 * The conversion type enum.
 *
 * @author Jesenko Mehmedbasic created 19-01-13, 14:21
 */
public enum ConversionType {
    Joining, Batch
}
