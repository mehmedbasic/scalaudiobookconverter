package dk.mehmedbasic.audiobook.conversion

import java.io.File

import dk.mehmedbasic.audiobook.execution.WorkerMessage.Result

/**
  * A batch conversion of audio files.
  */
trait Batch extends HandlesExistingFiles {
  def batchComplete(inputFiles: List[File], results: List[Result], outputFiles: List[File]) {
    for ((input, output) <- inputFiles zip outputFiles) {
      val filter: List[Result] = results.filter(r => input.equals(r.input))
      if (filter.isEmpty) {
        throw new IllegalStateException("Results doesn't contain all input files")
      }
      val dest: File = ensureFileNotOverwritten(filter.head.dest)
      dest.renameTo(output)
    }
  }


}
