package dk.mehmedbasic.audiobook.conversion

import java.io.OutputStream
import javazoom.jl.player.AudioDeviceBase

/**
  * A output stream writing audio device.
  *
  * @author Jesenko Mehmedbasic
  *         created 24-01-13, 22:46
  */
class OutputStreamAudioDevice(output: OutputStream) extends AudioDeviceBase {
  private var position = 0

  private var playing = true

  override def writeImpl(samples: Array[Short], offs: Int, len: Int): Unit = {
    samples.foreach(
      (s: Short) => {
        output.write((s.toInt >>> 8).toByte.toInt)
        output.write(s.toByte.toInt)
      })
    position += offs + len

    while (!playing) {
      Thread.sleep(150)
    }
  }

  def getPosition: Int = position

  override def closeImpl(): Unit = {
    output.close()
  }

  /**
    * Pauses the audio device
    */
  def pause(): Unit = {
    playing = false
  }

  /**
    * Plays the audio
    */
  def play(): Unit = {
    playing = true
  }
}
