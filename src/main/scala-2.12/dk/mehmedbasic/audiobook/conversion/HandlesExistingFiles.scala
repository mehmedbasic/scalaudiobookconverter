package dk.mehmedbasic.audiobook.conversion

import java.io.File

/**
  * TODO[jekm] - someone remind me to document this class.
  *
  * @author Jesenko Mehmedbasic
  *         created 5/7/14, 8:14 PM
  */
trait HandlesExistingFiles {
  def ensureFileNotOverwritten(dest: File): File = {
    var destinationFile: File = dest
    while (destinationFile.exists()) {
      destinationFile = new File(destinationFile.getAbsolutePath + "_1.m4b")
    }
    destinationFile
  }
}
