package dk.mehmedbasic.audiobook.conversion

import java.io.File

import dk.mehmedbasic.audiobook.event.EventPublisher
import dk.mehmedbasic.audiobook.execution.ConcatCommandFactory
import dk.mehmedbasic.audiobook.io.StreamReader

/**
  * Joins files
  * created 05-05-2014, 19:14
  */
trait Joining extends HandlesExistingFiles with EventPublisher {
  def mergeFiles(completedFiles: List[File], outputFile: File) :Unit={
    val factory: ConcatCommandFactory = new ConcatCommandFactory(completedFiles, outputFile)
    val process: Process = factory.createProcess()

    joining(outputFile)

    val reader: StreamReader = new StreamReader(process.getInputStream)
    reader.start()

    process.waitFor()

    new File(factory.temporaryOutput).renameTo(ensureFileNotOverwritten(outputFile))

    joiningFinished(outputFile)
    completedFiles.foreach((file: File) => file.delete())
  }
}
