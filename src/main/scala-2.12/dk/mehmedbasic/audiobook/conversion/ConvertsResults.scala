package dk.mehmedbasic.audiobook.conversion

import java.io.File

import dk.mehmedbasic.audiobook.execution.WorkerMessage.Result

import scala.collection.mutable

/**
  * Converts results from Result to files
  */
trait ConvertsResults {

  def convertResult(inputFiles: List[File], results: List[Result]): List[File] = {
    val result = new mutable.MutableList[File]()
    inputFiles.foreach(
      file => {
        val filter: List[Result] = results.filter(r => file.equals(r.input))
        if (filter.isEmpty) {
          throw new IllegalStateException("Results doesn't contain all input files")
        }
        result += filter.head.dest
      })
    result.toList
  }


}
