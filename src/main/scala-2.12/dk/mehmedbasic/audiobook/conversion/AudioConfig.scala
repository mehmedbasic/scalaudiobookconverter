package dk.mehmedbasic.audiobook.conversion

/**
  * Audio config.
  *
  * @author Jesenko Mehmedbasic
  *         created 19-01-13, 18:54
  */
case class AudioConfig(channels: Int = 2, sampleFrequency: Int = 44100, bits: Int = 16) {
}
