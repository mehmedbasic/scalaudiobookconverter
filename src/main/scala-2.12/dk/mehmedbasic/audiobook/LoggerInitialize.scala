package dk.mehmedbasic.audiobook

import java.io.InputStream
import java.util.Properties

import org.apache.log4j.PropertyConfigurator

object LoggerInitialize {
  def init(): Unit = {
    val properties: Properties = new Properties
    val in: InputStream = ClassLoader.getSystemClassLoader.getResourceAsStream("log4j.properties")
    properties.load(in)
    in.close()

    PropertyConfigurator.configure(properties)
  }

}
