package dk.mehmedbasic.audiobook.io

import java.io.{File, FileInputStream, InputStream}

import dk.mehmedbasic.audiobook.event.EventPublisher

/**
  * A counting input stream
  *
  * @author Jesenko Mehmedbasic
  *         created 27-01-13, 19:50
  */
class CountingInputStream(inputFile: File) extends InputStream with EventPublisher {
  val wrapped = new FileInputStream(inputFile)

  var bytesRead: Long = 0
  val bytesAvailable: Int = wrapped.available()

  var lastSeen: Double = -1d

  def read(): Int = {
    val currentRead: Int = wrapped.read()
    readBytes(currentRead)
    currentRead
  }


  override def read(b: Array[Byte]): Int = {
    val wrappedRead: Int = wrapped.read(b)
    readBytes(wrappedRead)
    wrappedRead
  }


  override def read(b: Array[Byte], off: Int, len: Int): Int = {
    val wrappedRead: Int = wrapped.read(b, off, len)
    readBytes(wrappedRead)
    wrappedRead
  }

  def readBytes(bytes: Int) :Unit={
    val percentage: Double = bytesRead.toDouble / available()
    val zeroToHundred: Double = math.round(percentage * 100) / 100d

    if (zeroToHundred != lastSeen) {
      //conversionTicking(inputFile, zeroToHundred)
      lastSeen = zeroToHundred
    }

    bytesRead += bytes
  }

  override def skip(n: Long): Long = wrapped.skip(n)

  override def available(): Int = bytesAvailable

  override def close(): Unit = wrapped.close()

  override def mark(readlimit: Int): Unit = wrapped.mark(readlimit)

  override def reset(): Unit = wrapped.reset()

  override def markSupported(): Boolean = wrapped.markSupported()

  def count: Long = bytesRead


}
