package dk.mehmedbasic.audiobook.io

import java.io.InputStream
import java.util.Scanner

class StreamReader(inputStream: InputStream) extends Thread {
  override def run(): Unit = {
    val scanner: Scanner = new Scanner(inputStream)
    while (scanner.hasNextLine) {
      scanner.nextLine
    }
  }
}
