package dk.mehmedbasic.audiobook

import org.apache.log4j.Logger

/**
  * TODO[jekm] - someone remind me to document this class.
  *
  * @author Jesenko Mehmedbasic
  *         created 5/5/14, 10:38 PM
  */
trait HasLogger {
  def logger(): Logger = Logger.getLogger(getClass)
}
