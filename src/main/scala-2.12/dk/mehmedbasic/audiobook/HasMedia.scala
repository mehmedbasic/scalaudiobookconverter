package dk.mehmedbasic.audiobook

import java.io.File

trait HasMedia {
  implicit class FileConverter(input: String) {
    def asFile(): File = {
      new File("media/" + input)
    }
  }
}
