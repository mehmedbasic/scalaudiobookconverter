organization := "dk.mehmedbasic"
name := "scalaudiobook"
version := "1.0"

scalaVersion := "2.12.2"

scalacOptions ++= Seq(
  "-deprecation",
  "-encoding", "UTF-8",
  "-feature",
  "-target:jvm-1.8",
  "-unchecked",
  "-Ywarn-adapted-args",
  "-Ywarn-dead-code",
  "-Ywarn-numeric-widen",
  "-Ywarn-unused",
  "-Ywarn-value-discard",
  "-Xfuture",
  "-Xlint"
)

javacOptions ++= Seq(
  "-Xlint:deprecation",
  "-Xlint:unchecked",
  "-source", "1.8",
  "-target", "1.8",
  "-g:vars"
)

libraryDependencies ++= Seq(
  "com.netaporter" % "scala-uri_2.12" % "0.4.16",
  "com.github.scopt" % "scopt_2.12" % "3.5.0",
  "org.blinkenlights.jid3" % "JID3" % "0.46",
  "net.sf.javamusictag" % "jid3lib" % "0.5.4",
  "com.googlecode.soundlibs" % "jlayer" % "1.0.1.4",
  "log4j" % "log4j" % "1.2.17",
  "org.slf4j" % "slf4j-log4j12" % "1.7.25",
  "com.typesafe.akka" % "akka-actor_2.12" % "2.4.17",
  "junit" % "junit" % "4.12" % "test"
)

logLevel := Level.Warn
logLevel in compile := Level.Warn
logLevel in test := Level.Info

